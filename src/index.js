/**
 * Created by front on 2017-06-19.
 */
//import md from 'markdown-it';
// full options list (defaults)
var md = require('markdown-it')({
    html:         false,        // Enable HTML tags in source
    xhtmlOut:     false,        // Use '/' to close single tags (<br />).
                                // This is only for full CommonMark compatibility.
    breaks:       true,        // Convert '\n' in paragraphs into <br>
    langPrefix:   'language-',  // CSS language prefix for fenced blocks. Can be
                                // useful for external highlighters.
    linkify:      false,        // Autoconvert URL-like text to links

    // Enable some language-neutral replacement + quotes beautification
    typographer:  false,

    // Double + single quotes replacement pairs, when typographer enabled,
    // and smartquotes on. Could be either a String or an Array.
    //
    // For example, you can use '«»„“' for Russian, '„“‚‘' for German,
    // and ['«\xA0', '\xA0»', '‹\xA0', '\xA0›'] for French (including nbsp).
    quotes: '“”‘’',

    // Highlighter function. Should return escaped HTML,
    // or '' if the source string is not changed and should be escaped externaly.
    // If result starts with <pre... internal wrapper is skipped.
    highlight: function (/*str, lang*/) { return ''; }
});

$(function () {
    $('textarea').on('keyup', function(e){


        console.log(e);
        console.log($(this).val());
        let result = md.render($(this).val());
        $('.result-html').html(result);

    });


    $('textarea').on('drop', function(event, ui){
        console.log($(this).val());
        let result = md.render($(this).val());
        $('.result-html').html(result);
       console.log(event, this);
        event.preventDefault();

        var myValue ='![image](https://ssproxy.ucloudbiz.olleh.com/v1/AUTH_74113319-8894-45bb-82c1-4663d1c7f789/maxst/wordpress/images3D/doc/GetStarted/image_2.png)';
        setSelectionRange(myValue, this);
        var files = event.originalEvent.dataTransfer.files;

        console.log(files);

        //Use FormData to send the files
    //    var formData = new FormData();

     //   formData.append('files', files[0]);
    });



    $('textarea, .result-html').on('scroll', scrollSunc);

});
var timeout;
function scrollSunc(){
    clearTimeout(timeout);

    var el = $(this);
    var target = $(el.is('textarea') ? '.result-html':'textarea');

    target.off('scroll').scrollTop($(this).scrollTop());
    timeout = setTimeout(function(){
        target.on('scroll', scrollSunc);
    },100);
}

function setSelectionRange(value, el) {
    if (document.selection) {
        //For browsers like Internet Explorer
        el.focus();
        var sel = document.selection.createRange();
        sel.text = value;
        this.focus();
    }
    else if (el.selectionStart || el.selectionStart == '0') {
        //For browsers like Firefox and Webkit based
        var startPos = el.selectionStart;
        var endPos = el.selectionEnd;
        var scrollTop = el.scrollTop;
        el.value = el.value.substring(0, startPos)+value+el.value.substring(endPos,el.value.length);
        el.focus();
        el.selectionStart = startPos + value.length;
        el.selectionEnd = startPos + value.length;
        el.scrollTop = scrollTop;
        $(el).keyup();
    } else {
        el.value += value;
        el.focus();
        $(el).keyup();
    }
}

/*
var dropzone = document.getElementById('ta'); // paste your dropzone id here
dropzone.ondrop = function(e){
    console.log('drop'); // for debugging reasons
    e.preventDefault();  // stop default behaviour
};
*/



if (module.hot) { // hot module replace plugin 은 이 설정이 필요
    module.hot.accept();
}