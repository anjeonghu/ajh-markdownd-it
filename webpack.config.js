/**
 * Created by front on 2017-06-19.
 */
var webpack = require('webpack');
module.exports = {
    entry: './src/index.js',
    output: {
        path: __dirname + '/public/',
        filename: 'bundle.js'
    },
    devServer: {
        hot:true,
        inline: true,
        host:'0.0.0.0',
        port: 5000,
        contentBase: __dirname + '/public/'
    },
    module: {
        rules: [{
            test: /\.jsx?$/,
            loader: 'babel-loader',
            options: {
                presets: [[ 'es2015', { modules: false } ]],
            },
            exclude: ['/node_modules'],
        }],
    },
    plugins: [
    ],
    resolve: {
        modules: ['node_modules'],
        extensions: ['.js', '.json', '.jsx', '.css'],
    },
};